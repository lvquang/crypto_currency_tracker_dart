import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';

class CryptoModel {
  final String id;
  final String name;
  final String symbol;
  final String priceUsd;
  final String percentChange1h;
  final String percentChange24h;
  final String percentChange7d;

  CryptoModel(
      {this.id,
      this.name,
      this.symbol,
      this.priceUsd,
      this.percentChange1h,
      this.percentChange24h,
      this.percentChange7d});

  factory CryptoModel.fromJson(Map<String, dynamic> json) {
    return new CryptoModel(
      id: json['id'] as String,
      name: json['name'] as String,
      symbol: json['symbol'] as String,
      priceUsd: json['price_usd'] as String,
      percentChange1h: json['percent_change_1h'] as String,
      percentChange24h: json['percent_change_24h'] as String,
      percentChange7d: json['percent_change_7d'] as String,
    );
  }
}

Future<List<CryptoModel>> _fetchCurrencies(
    http.Client client, int index) async {
  final response = await client
      .get('https://api.coinmarketcap.com/v1/ticker/?start=$index&limit=10');
  return compute(parseCurrencies, response.body);
}

// A function that will convert a response body into a List<Photo>
List<CryptoModel> parseCurrencies(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed
      .map<CryptoModel>((json) => new CryptoModel.fromJson(json))
      .toList();
}

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Crypto currency tracker';

    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(
        title: appTitle,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  List<CryptoModel> _cryptoList = [];
  ScrollController _scrollController = new ScrollController();
  bool _loading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _handleRefresh();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print('scroll to bottom');
        _getDataAtIndex(_cryptoList.length + 10);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement buildq
    print('build build');
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text(widget.title),
          actions: <Widget>[
            new IconButton(
                icon: const Icon(Icons.refresh),
                tooltip: 'Refresh',
                onPressed: () {
                  _handleRefresh();
                }),
          ],
        ),
        body: new RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _handleRefresh,
            child: _loading
                ? new Center(
                    child: new CircularProgressIndicator(),
                  )
                : new ListView.builder(
                    itemCount: _cryptoList.length +
                        (_cryptoList.length % 10 == 0 ? 1 : 0),
                    itemBuilder: (context, index) {
                      if (index == _cryptoList.length) {
                        return _buildProgressIndicator();
                      }
                      return new CurrencyListItem(item: _cryptoList[index]);
                    },
                    controller: _scrollController,
                  )));
  }

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
          child: new Opacity(
        opacity: 1.0,
        child: new CircularProgressIndicator(),
      )),
    );
  }

  Future<Null> _getDataAtIndex(int index) async {
    print('_getDataAtIndex $index');
    print('_loading $_loading');

    if (!_loading) {
      _loading = true;
      // _fetchCurrencies(new http.Client(), index)
      //     .then((list) => _addCryptoToList(list));
      List<CryptoModel> list = await _fetchCurrencies(new http.Client(), index);
      if (list.isEmpty) {
        double edge = 50.0;
        double offsetFromBottom = _scrollController.position.maxScrollExtent -
            _scrollController.position.pixels;
        if (offsetFromBottom < edge) {
          _scrollController.animateTo(
              _scrollController.offset - (edge - offsetFromBottom),
              duration: new Duration(milliseconds: 500),
              curve: Curves.easeOut);
        }
      }

      setState(() {
        _cryptoList.addAll(list);
        _loading = false;
        print('Updated list');
      });
    }
  }

  //   Future<Null> _handleRefresh() async {
  //   Completer<Null> completer = new Completer<Null>();
  //   if (!_loading) {
  //     _loading = true;
  //     _fetchCurrencies(new http.Client(), 0)
  //         .then((list) => _addCryptoToList(list));
  //     completer.complete();
  //     _loading = false;
  //   }
  //   return completer.future;
  // }

  Future<Null> _handleRefresh() async {
    print('_handleRefresh');
    _cryptoList = [];
    _getDataAtIndex(0);
  }
}

class CurrencyListItem extends StatelessWidget {
  final CryptoModel item;

  CurrencyListItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Column(
      children: <Widget>[
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Row(
            children: <Widget>[
              new CachedNetworkImage(
                  imageUrl:
                      "https://res.cloudinary.com/dxi90ksom/image/upload/${item.symbol}",
                  placeholder: new CircularProgressIndicator(),
                  errorWidget: new Icon(Icons.error),
                  width: 50.0,
                  height: 50.0),
              new Expanded(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Expanded(
                        child: new Container(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Text('${item.symbol}',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold)),
                              new Text(' | '),
                              new Text('${item.name}'),
                            ],
                          ),
                          new Row(
                            children: <Widget>[
                              new Text('1h: '),
                              new Text(
                                '${item.percentChange1h}%',
                                style: new TextStyle(color: Colors.green),
                              ),
                              new Padding(
                                  padding: const EdgeInsets.only(
                                      left: 4.0, right: 4.0)),
                              new Text('24h: '),
                              new Text(
                                '${item.percentChange24h}%',
                                style: new TextStyle(color: Colors.red),
                              ),
                            ],
                          )
                        ],
                      ),
                    )),
                    new Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text('\$\ ${item.priceUsd}'),
                          new Row(
                            children: <Widget>[
                              new Text('7d: '),
                              double.parse(item.percentChange7d) > 0
                                  ? new Text('${item.percentChange7d} %',
                                      style: new TextStyle(color: Colors.green))
                                  : new Text('${item.percentChange7d} %',
                                      style: new TextStyle(color: Colors.red))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        new Divider()
      ],
    );
  }
}
